#!/bin/bash

useradd test1
useradd test2
useradd test3
sudo echo "123" | passwd --stdin test1
sudo echo "123" | passwd --stdin test2
sudo echo "123" | passwd --stdin test3
groupadd admin

usermod -aG admin test1
usermod -aG admin test2

echo "*;*;*;!Wd0000-2400" >> /etc/security/time.conf
echo "+:vagrant:ALL" >> /etc/security/access.conf
echo "+:admin:ALL" >> /etc/security/access.conf
echo "-:ALL:ALL" >> /etc/security/access.conf

sed -i '6 i account    [success=1 default=ignore]     pam_access.so' /etc/pam.d/login
sed -i '7 i account    required     pam_time.so' /etc/pam.d/login

sudo sed -i '7 i account    [success=1 default=ignore]     pam_access.so' /etc/pam.d/sshd
sudo sed -i '8 i account    required     pam_time.so' /etc/pam.d/sshd

groupadd docker
usermod -aG docker test1

yum -y install docker
systemctl enable docker
systemctl start docker

echo "test1 ALL=NOPASSWD:/bin/systemctl restart docker.service, /bin/systemctl restart docker, /bin/systemctl status docker.service, /bin/systemctl status docker" >> /etc/sudoers

systemctl restart docker

