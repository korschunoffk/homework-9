# Работа с пользователями - ограничить доступ всех пользователей кроме группы admin в выходные дни
создадим троих пользователей, назначим им пароли и добавим первых двоих в группу admin

```
useradd test1
useradd test2
useradd test3
echo "123" | passwd --stdin test1
echo "123" | passwd --stdin test2
echo "123" | passwd --stdin test3
groupadd admin
usermod -aG admin test1
usermod -aG admin test2
```

Для наложения тербуемых ограничени требуется внести правки в `/etc/pam.d/login` и ` /etc/pam.d/sshd` - политики отвечающие за логин и подключение по ssh

необходимо поправить конфигурации `pam_time.so` и `pam_access.so`, которые расположены в `/etc/security/time.conf` и `/etc/security/access.conf`


Запрет на вход на выходных всем .
```
echo "*;*;*;!Wd0000-2400" >> /etc/security/time.conf

```
Разрешение на вход группе admin и запрет всем остальным. 

```
echo "+:admin:ALL" >> /etc/security/access.conf
echo "-:ALL:ALL" >> /etc/security/access.conf
```

непосредственно сами правки в `/etc/pam.d/login`
```
sed -i '6 i account    [success=1 default=ignore]     pam_access.so' /etc/pam.d/login
sed -i '7 i account    required     pam_time.so' /etc/pam.d/login
```
стоит обратить внимание на строку `[success=1 default=ignore]` - в случае успешного выполнения (если пользователю разрешен вход, в частности группе admin), то пропустить одно 
следующее правило , pam_time.so . во всех остальных случаях игнорировать результат выполнения и переходить к следующему правилу.

аналогично для `/etc/pam.d/sshd`
```
sudo sed -i '7 i account    [success=1 default=ignore]     pam_access.so' /etc/pam.d/sshd
sudo sed -i '8 i account    required     pam_time.so' /etc/pam.d/sshd
```

# Доп задание - дать пользователю права на докер и права рестартовать Докер

Создаем группу докер и добавляем туда пользовате test1 - это права на работу с докером.
```
groupadd docker
usermod -aG docker test1
```

Устанавливаем докер 

```
yum -y install docker
systemctl enable docker
systemctl start docker
```
Разрешаем рестартовать Докер путем внесения пользователя в файл suders.
Обязательно пишем корректное правило через visudo.
**В скрипт включаем только вариант прошедший через visudo !!**

```
echo "test1 ALL=NOPASSWD:/bin/systemctl restart docker.service, /bin/systemctl restart docker, /bin/systemctl status docker.service, /bin/systemctl status docker" >> /etc/sudoers

```
рестартуем Докер на всякий.

```
systemctl restart docker
```

